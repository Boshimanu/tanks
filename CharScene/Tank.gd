extends KinematicBody2D

export (int) var speed
export (float) var turret_speed_rotation 
export (float) var gun_colldown 
export (int) var health

var velocity = Vector2()
var can_shoot = true
var alive = true

func _ready():
	$FireDelay.wait_time = gun_colldown

func _physics_process(delta):
	if !alive:
		return
	control(delta)
	$TankTurret.rotation_degrees += 90 # - Fixing the rotation
	velocity = move_and_slide(velocity)

func control(delta):
	pass