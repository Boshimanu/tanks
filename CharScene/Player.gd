extends "res://CharScene/Tank.gd"

func control(delta):
	var turret_dir = 0
	$TankTurret.look_at(get_global_mouse_position())
	
	if Input.is_action_pressed("ui_left"):
		turret_dir = -1
	elif Input.is_action_pressed("ui_right"):
		turret_dir = 1
	rotation += turret_dir * turret_speed_rotation * delta
	velocity = Vector2()
	if Input.is_action_pressed("ui_down"):
		velocity = Vector2(0, speed).rotated(rotation)
	elif Input.is_action_pressed("ui_up"):
		velocity = Vector2(0, -speed).rotated(rotation)